#import "Specta.h"
#define EXP_SHORTHAND
#import "Expecta.h"

@class XCTestSuiteRun;
#define RunSpec(TestClass) RunSpecClass([TestClass class])
XCTestSuiteRun *RunSpecClass(Class testClass);

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>
#define MOCKITO_SHORTHAND
#import <OCMockito/OCMockito.h>
