#import "TestHelper.h"
#import "Game.h"

SpecBegin(GameSpec)

describe(@"A Game", ^{

    __block Game *game;

    before(^{
        game = [[Game alloc] init];
    });

    after(^{
        game = nil;
    });

    it(@"should initialize itself", ^{
        expect(game).toNot.beNil();
    });

    it(@"should score 0 when rolling a gutter game", ^{
        for (int i = 0; i < 20; i++) {
            [game rollWithPinCount:0];
        }
        expect([game score]).to.equal(0);
    });

    it(@"should score 20 when rolling all ones", ^{
        for (int i = 0; i < 20; i++) {
            [game rollWithPinCount:1];
        }
        expect([game score]).to.equal(20);
    });

    it(@"should score 16 when rolling 1 spare", ^{
        [game rollWithPinCount:5];
        [game rollWithPinCount:5]; // spare
        [game rollWithPinCount:3];
        for (int i = 0; i < 17; i++)
            [game rollWithPinCount:0];
        expect([game score]).to.equal(16);
    });

    it(@"should score 24 when rolling 1 strike", ^{
        [game rollWithPinCount:10];
        [game rollWithPinCount:3];
        [game rollWithPinCount:4];
        for (int i = 0; i < 16; i++)
            [game rollWithPinCount:0];
        expect([game score]).to.equal(24);
    });

    it(@"should sore 300 when rolling a perfect game (12 strikes in a row)", ^{
        for (int i = 0; i < 12; i++)
            [game rollWithPinCount:10];
        expect([game score]).to.equal(300);
    });
});

SpecEnd