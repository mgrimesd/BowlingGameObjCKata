//
//  AppDelegate.h
//  BowlingGameObjCKata
//
//  Created by Mark Grimes on 2/9/14.
//  Copyright (c) 2014 Stateful Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end