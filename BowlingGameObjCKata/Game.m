#import "Game.h"

@implementation Game {
    int rolls[21];
    int currentRoll;
}

- (void)rollWithPinCount:(int)pins {
    rolls[currentRoll++] = pins;
}

- (int)score {
    int score = 0;
    int frameIndex = 0;
    for (int frame = 0; frame < 10; frame++) {
        if ([self isStrike:frameIndex]) {
            score += 10 + [self strikeBonus:frameIndex];
            frameIndex++;
        } else if ([self isSpare:frameIndex]) {
            score += 10 + [self spareBonus:frameIndex];
            frameIndex += 2;
        } else {
            score += [self sumOfBallsInFrame:frameIndex];
            frameIndex += 2;
        }
    }
    return score;
}

- (int)sumOfBallsInFrame:(int)frameIndex {
    return rolls[frameIndex] + rolls[frameIndex + 1];
}

- (int)spareBonus:(int)frameIndex {
    return rolls[frameIndex + 2];
}

- (int)strikeBonus:(int)frameIndex {
    return rolls[frameIndex + 1] + rolls[frameIndex + 2];
}

- (BOOL)isStrike:(int)frameIndex {
    return rolls[frameIndex] == 10;
}

- (BOOL)isSpare:(int)frameIndex {
    return rolls[frameIndex] + rolls[frameIndex + 1] == 10;
}

@end