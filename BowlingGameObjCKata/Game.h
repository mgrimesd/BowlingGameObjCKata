#import <Foundation/Foundation.h>

@interface Game : NSObject

- (void)rollWithPinCount:(int)pins;
- (int)score;

@end